import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MessageService {

  url = 'http://localhost/portfolio/uploadmessage.php';
  constructor(private _http: HttpClient) { }

  sendMessage(messageData) {
    return this._http.post<any>(this.url, messageData);
  }
  

}

