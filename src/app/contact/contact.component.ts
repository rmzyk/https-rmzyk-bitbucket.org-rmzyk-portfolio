import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { pipe } from 'rxjs';
import { MessageService } from '../common/message.service';
import { HttpErrorResponse } from '@angular/common/http';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent {

  constructor(private formBuilder : FormBuilder,
              private _messageService: MessageService) {
   }

   contactForm = this.formBuilder.group({
      name: '',
      surname: '',
      email: '',
      company: '',
      message: ''
     })
  

  sendMessage(){
  console.log(this.contactForm.value);
  this._messageService.sendMessage(this.contactForm.value)
  .subscribe(
    response => console.log(response),
    (err: HttpErrorResponse) => {
      console.log(err.error);
      console.log(err.name);
      console.log(err.message);
      console.log(err.status);
    }
  );
  }
}
