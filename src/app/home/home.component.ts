import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  birthDate: Date = new Date("1997-10-15");
  
 Age : number;

calcAge(birthDate) {
  var birthday = +new Date(birthDate);
  this.Age = (Date.now() - birthday) / (31557600000);
}
  constructor() { }

  ngOnInit() {
    this.calcAge(this.birthDate);
  }

}
